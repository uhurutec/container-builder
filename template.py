#!/usr/bin/python3
# Copyright (C) 2021-2022 InterFace AG
#
# This program is free software: you can redistribute it and/or modify it und
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

"""
Templating Logic

This program takes a template file and replaces all ocurrences of %%VAR%% with
the content of the variable VAR from the enviroment and writes the result to
the output.

Note: Variables which are not found in the environment will not be replaced.
      Nested variables are not supported.
"""

import os
from argparse import ArgumentParser

parser = ArgumentParser(description=__doc__)
parser.add_argument("template")
parser.add_argument("output")

parameter = parser.parse_args()

with open(parameter.template) as handle:
    template = handle.read()

output = []
replacing = False
delimiter = "%%"
for part in template.split(delimiter):
    if replacing:
        if part in os.environ:
            output.append(os.environ[part])
        else:
            output.append(delimiter)
            output.append(part)
            output.append(delimiter)
    else:
        output.append(part)
    replacing = not replacing

with open(parameter.output, "w") as handle:
    handle.write(''.join(output))

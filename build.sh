#!/bin/sh
# Copyright (C) 2021-2022 InterFace AG
#
# This program is free software: you can redistribute it and/or modify it und
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


set -e

# Make sure that variables we use are in an initialized state.
parameters=""
docker_proxy_config=""
push=no

# make sure the container tag meets specification
# ref. https://docs.docker.com/engine/reference/commandline/tag/
normalize_tag() {
  CB_TAG=$(printf "%s" "$CB_TAG" | sed 's|[^a-zA-Z0-9\.\-]|_|g;s|^[\.\-]|_|' | awk '{ string=substr($0, 1, 127); print string; }')
}

# Pipe in the output of docker login to ignore the message about credential helpers
ignore_login_lines() {
  line1='WARNING! Your password will be stored unencrypted in /root/.docker/config.json.'
  line2='Configure a credential helper to remove this warning. See'
  line3='https://docs.docker.com/engine/reference/commandline/login/#credentials-store'
  while read -r line; do
    for reference in "$line1" "$line2" "$line3"; do
      if [ "$reference" = "$line" ]; then
        continue 2
      fi
    done
    echo "$line"
  done
}

if [ -n "$CB_HOOK_PRE_BUILD" ]; then
  if ! [ -d "$CB_HOOK_PRE_BUILD" ]; then
    echo "hooks directory set by CB_HOOK_PRE_BUILD wasn't found" >&2
    exit 1
  fi
  for hook in "$CB_HOOK_PRE_BUILD"/*; do
    printf '[\e[93mPRE BUILD HOOK\e[0m] executing %s\n' "$hook"
    # shellcheck disable=SC1090
    . "$hook"
    printf '[\e[93mPRE BUILD HOOK\e[0m] finished %s\n' "$hook"
  done
fi

if [ -z "$CB_REGISTRY" ]; then
  if [ -n "$CB_REGISTRY_IMAGE" ]; then
    CB_REGISTRY="${CB_REGISTRY_IMAGE%%/*}"
  elif [ -z "$CI_REGISTRY" ]; then
    echo "Registry undefined. Please set CB_REGISTRY!" >&2
    exit 1
  else
    CB_REGISTRY="$CI_REGISTRY"
  fi
fi
echo "Using \"$CB_REGISTRY\" as container registry."

if ! [ -d "$CI_PROJECT_DIR" ]; then
  echo "Assertion failed: CI_PROJECT_DIR is not set or does not exist." >&2
  exit 1
fi

if [ -z "$CB_MAIN_BRANCH" ]; then
  if [ -n "$CI_DEFAULT_BRANCH" ]; then
    CB_MAIN_BRANCH="$CI_DEFAULT_BRANCH"
  else
    CB_MAIN_BRANCH=master
  fi
fi

if [ -n "$CB_TAG" ]; then
  push=yes
else
  if [ -n "$CI_COMMIT_TAG" ]; then
    CB_TAG="$CI_COMMIT_TAG"
    push=yes
  elif [ -n "$CI_COMMIT_BRANCH" ]; then
    if [ "$CI_COMMIT_BRANCH" = "$CB_MAIN_BRANCH" ]; then
      CB_TAG=latest
      push=yes
    else
      CB_TAG="$CI_COMMIT_BRANCH"
    fi
  elif [ -n "$CI_COMMIT_REF_NAME" ]; then
    echo "Using CI_COMMIT_REF_NAME \"$CI_COMMIT_REF_NAME\" for tagging." >&2
    CB_TAG="$CI_COMMIT_REF_NAME"
  else
    echo "Assertion failed: neither CI_COMMIT_TAG, CI_COMMIT_BRANCH nor CI_COMMIT_REF_NAME is set." >&2
    exit 1
  fi
fi

if [ -z "$CB_REGISTRY_IMAGE" ]; then
  if [ -z "$CB_IMAGE" ]; then
    echo "No image name specified. Please set either CB_REGISTRY_IMAGE or CB_IMAGE!" >&2
    exit 1
  else
    CB_REGISTRY_IMAGE="$CB_REGISTRY/$CB_IMAGE"
  fi
fi

if [ -z "$CB_IMAGE" ]; then
  CB_IMAGE="${CB_REGISTRY_IMAGE#"$CB_REGISTRY/"}"
fi

if [ -n "$CB_HOOK_POST_BUILD" ]; then
  if ! [ -d "$CB_HOOK_POST_BUILD" ]; then
    echo "hooks directory set by CB_HOOK_POST_BUILD wasn't found" >&2
    exit 1
  fi
fi

# add CA certificate to docker config

if [ -z "$CB_CA_CERT" ]; then
  CB_CA_CERT=/certs/ca.crt
fi

if [ -f "$CB_CA_CERT" ]; then
  mkdir -p "/etc/docker/certs.d/$CB_REGISTRY"
  cp "$CB_CA_CERT" "/etc/docker/certs.d/$CB_REGISTRY/ca.crt"
fi

# handle proxy settings

if [ -n "$CB_HTTP_PROXY" ]; then
  docker_proxy_config="\"httpProxy\": \"$CB_HTTP_PROXY\""
  parameters="$parameters --build-arg http_proxy=$CB_HTTP_PROXY"
fi

if [ -n "$CB_HTTPS_PROXY" ]; then
  if [ -n "$docker_proxy_config" ]; then
    docker_proxy_config="$docker_proxy_config, "
  fi
  docker_proxy_config="$docker_proxy_config\"httpsProxy\": \"$CB_HTTPS_PROXY\""
  parameters="$parameters --build-arg https_proxy=$CB_HTTPS_PROXY"
fi

if [ -n "$CB_NO_PROXY" ]; then
  if [ -n "$docker_proxy_config" ]; then
    docker_proxy_config="$docker_proxy_config, "
  fi
  docker_proxy_config="$docker_proxy_config\"noProxy\": \"$CB_NO_PROXY\""
  parameters="$parameters --build-arg no_proxy=$CB_NO_PROXY"
fi

if [ -n "$docker_proxy_config" ]; then
  docker_proxy_config="\"proxies\": { \"default\": { $docker_proxy_config } }"
  mkdir -p /cbuilder/.docker
  echo "{ $docker_proxy_config }" >/cbuilder/.docker/config.json
fi

# handle credentials for registry and login
if [ -n "$CB_REGISTRY_USERNAME" ] && [ -n "$CB_REGISTRY_PASSWORD" ]; then
  echo "$CB_REGISTRY_PASSWORD" \
    | docker login "$CB_REGISTRY" --username "$CB_REGISTRY_USERNAME" --password-stdin 2>&1 \
    | ignore_login_lines
elif [ -z "$CB_REGISTRY_USERNAME" ] && [ -z "$CB_REGISTRY_PASSWORD" ]; then
  echo "Assuming authentication with registry is not required."
else
  echo "Username or password missing for container registry authentication."
  echo "Set both CB_REGISTRY_USERNAME and CB_REGISTRY_PASSWORD to authenticate."
fi

# handle build args
for variable in $CB_BUILD_ARGS; do
  parameters="$parameters --build-arg $variable"
done

cd "$CI_PROJECT_DIR" || exit 1

normalize_tag
echo "Using tag $CB_TAG"

# fall back to /Dockerfile as default
if [ -z "$CB_DOCKER_FILE" ]; then
  CB_DOCKER_FILE="$CI_PROJECT_DIR/Dockerfile"
fi

CB_LOCAL_IMAGE="$CB_IMAGE:$CB_TAG"

# put possibly modified variables from this script into environment
export CB_IMAGE CB_TAG CB_DOCKER_FILE CB_REGISTRY_IMAGE CB_MAIN_BRANCH CB_REGISTRY CB_LOCAL_IMAGE

if [ -n "$CB_DOCKER_TEMPLATE" ]; then
  if ! [ -f "$CB_DOCKER_TEMPLATE" ]; then
    echo "Could not find given dockerfile template file" >&2
    exit 1
  fi
  if [ -f "$CB_DOCKER_FILE" ]; then
    echo "Warning: dockerfile $CB_DOCKER_FILE already exists. Overwriting!" >&2
  fi

  /template.py "$CB_DOCKER_TEMPLATE" "$CB_DOCKER_FILE"
fi

if ! [ -f "$CB_DOCKER_FILE" ]; then
  echo "Could not find a dockerfile. Please set CB_DOCKER_FILE!" >&2
  exit 1
fi

# we want the parameters to expand here:
# shellcheck disable=SC2086
docker build $parameters --no-cache --file "$CB_DOCKER_FILE" --tag "$CB_LOCAL_IMAGE" .

if [ -n "$CB_HOOK_POST_BUILD" ]; then
  for hook in "$CB_HOOK_POST_BUILD"/*; do
    printf "[\e[93mPOST BUILD HOOK\e[0m] executing %s\n" "$hook"
    # shellcheck disable=SC1090
    . "$hook"
    printf "[\e[93mPOST BUILD HOOK\e[0m] finished %s\n" "$hook"
  done
fi

if [ "$push" = "yes" ]; then
  docker image tag "$CB_LOCAL_IMAGE" "$CB_REGISTRY_IMAGE:$CB_TAG"
  docker push "$CB_REGISTRY_IMAGE:$CB_TAG"

  if [ -n "$CB_METADATA_FILE" ]; then
    image_by_tag="$(docker inspect --format='{{index .RepoDigests 0}}' "$CB_REGISTRY_IMAGE:$CB_TAG")"
    {
      echo "by_tag: $CB_REGISTRY_IMAGE:$CB_TAG"
      echo "by_sha256: $image_by_tag"
    } > "${CB_METADATA_FILE}"
  fi
fi

if [ -n "$CB_ADDITIONAL_TAGS" ]; then
  (
    IFS=':'
    for additional_tag in $CB_ADDITIONAL_TAGS; do
      additional_image="$CB_REGISTRY_IMAGE:$additional_tag"
      echo " * Creating additional tag \"$additional_image\"."
      docker image tag "$CB_LOCAL_IMAGE" "$additional_image"
      docker push "$additional_image"
    done
  )
fi

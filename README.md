# Container Builder

Build container images from within a Gitlab-CI pipeline and deploy to your
registry.

## Description

This container is based on the [docker](https://hub.docker.com/_/docker)
container.

In contrary to the described way in the [Gitlab documentation][gldoc], this
approach does not require the *docker-in-docker* service, but relies on the
runner to be configured to use the hosts' docker service, by exposing the docker
socket `/var/run/docker.sock` to the runners' docker executor.

For cleaner and more general `gitlab-ci.yml` files in your code repositories,
this container can be controlled via a set of environment variables.

[gldoc]: https://docs.gitlab.com/ce/ci/docker/using_docker_build.html

## Usage

The build is controlled by a set of environment variables. Some variables are
set automatically by the Gitlab runner and are prefixed with `CI`.

- `CI_REGISTRY`
  This variable is set, if the Gitlab integrated container registry is enabled,
  otherwise you should set this variable in your Gitlab instance to build this
  container. Use `CB_REGISTRY` to override.

- `CI_PROJECT_DIR`
  This variable is set to the checkout of the current git repository.

- `CI_COMMIT_TAG`
  This variable is set to the git tag, if the pipeline is run on a tag. This
  will be used to tag the built image on the registry. Use `CB_TAG` to override.

- `CI_COMMIT_BRANCH`
  This variable is set to the branch name. This will be used, if `CI_COMMIT_TAG`
  is not set. Use `CB_TAG` to override.

In addition, variables prefixed with `CB` can be used to override some of
the `CI` variables or control other aspects of the build. These variables can be
set in the `.gitlab.yml` manifest or in Gitlab globally or at project and group
level.

- `CB_REGISTRY`
  Set the registry where the resulting image will be pushed to. If unset, the
  build script will try to extract the registry from `CB_REGISTRY_IMAGE`. As a
  fallback it will use `CI_REGISTRY`, which will be set, if Gitlab integrated
  registry is enabled.

- `CB_REGISTRY_USERNAME` and `CB_REGISTRY_PASSWORD`
  If *both* variables are set the build script will invoke `docker login` before
  building and pushing the container image.

- `CB_INSECURE_REGISTRY`
  Set this variable to a non-empty string to enable pushing to the registry
  without SSL.

- `CB_CA_CERT`
  Set this to the CA certificate of your container registry if needed. If this
  variable is not set the build script will try to use `/certs/ca.crt`.

- `CB_HTTP_PROXY`
  Set the proxy for HTTP. Example: `http://127.0.0.1:3001`

- `CB_HTTPS_PROXY`
  Set the proxy for HTTPS. Example: `http://127.0.0.1:3001`

- `CB_NO_PROXY`
  Set domain which can be accessed without proxy.
  Example: `*.test.example.com,.example2.com`

- `CB_DOCKER_FILE`
  Set the location of the Dockerfile. If not set, the build script assumes the
  file `Dockerfile` in the repository root.

- `CB_DOCKER_TEMPLATE`
  Set the location of a template for the Dockerfile. The dockerfile will be
  created from the template by replacing statements `%%VARIABLE%%` with the
  respective variable from the environment. Note: there is no safeguard of any
  kind preventing bad things to happen. Variables which are not found in the
  environment will not be replaced. Nested variables are not supported.

- `CB_REGISTRY_IMAGE`
  Set the full image name without the tag. In effect the image will be tagged
  to `$CB_REGISTRY_IMAGE:$CB_TAG`. If this is not set it will be deducted by
  combing `CB_REGISTRY` and `CB_IMAGE`.

- `CB_IMAGE`
  The image name of the built image. Set this as an alternative for setting the
  full name via `CB_REGISTRY_IMAGE`. In effect the image will be tagged to
  `$CB_REGISTRY/$CB_IMAGE:$CB_TAG`. Note that `CB_REGISTRY_IMAGE` takes
  precedence.

- `CB_TAG`
  The built image will be tagged with this tag. If unset, `CI_COMMIT_TAG` or if
  that is unset as well `CI_COMMIT_BRANCH` will be used. Note that if you want
  access the image without tags, you have to set this variable explicitly
  to `latest`.

- `CB_MAIN_BRANCH`
  Set the branch, which is considered the main branch. If `CI_COMMIT_BRANCH`
  matches this branch, then the image will be tagged `latest`, if `CB_TAG` isn't
  set. If this variable is not set, then `master` is considered main branch.

- `CB_ADDITIONAL_TAGS`
  This variable is a colon-separated list of tags. The built image will be
  additionally tagged with each tag of the list and pushed to the registry.

- `CB_METADATA_FILE`
  If set, after a successful push an artifact description is emitted to
  `CB_METADATA_FILE`.

- `CB_BUILD_ARGS`
  Set environment variables to be passed to the build process. The content is a
  space-separated list of variable names. Use the ARG statement in your
  Dockerfile for the variable to be set during build

- `CB_HOOK_PRE_BUILD` and `CB_HOOK_POST_BUILD`
  These can be used to specify a directory, from which hook scripts are
  executed. Refer to section [Hooks](#hooks) for the usage of hooks.

At least either `CB_REGISTRY_IMAGE` or `CB_IMAGE` is required to be set. If the
Gitlab integrated container registry isn't enabled, you also should set
`CI_REGISTRY` or `CB_REGISTRY` globally in your Gitlab instance. (see also
[Rationale](#rationale)).

Note that the image is only pushed to the container registry if

* the image was built on the main branch (usually `master`),
* the image was built on a git tag,
* the image tag is explicitly set via `CB_TAG` or
* `CB_ADDITIONAL_TAGS` is set.

## Bootstrap

Build the container by hand. Set or replace `CB_REGISTRY` variable with your
registry.

```
docker build -t $CB_REGISTRY/container-builder .
```

Then push the image to your registry.

```
docker push $CB_REGISTRY/container-builder
```

## Rationale

Since docker images have the container registry hard-wired in their names, you
would have to also put your registry in the `.gitlab-ci.yml` manifest. In order
to abstract the manifest file to some degree from the infrastructure we use the
environment variables above.

The registry will be set by Gitlab Runner in `CI_REGISTRY`, if the Gitlab
integrated registry is used. This build container also introduces `CB_REGISTRY`
and `CB_REGISTRY_IMAGE` as a way to override this variable as needed.

These are distinct variables since

* there is no [fallback support for variables][var-defaults] and
* this allows using the container-builder image from `CI_REGISTRY` while pushing
  to `CB_REGISTRY`

Since `CI_REGISTRY` is automatically set by Gitlab if container registry is
enabled, `CB_REGISTRY` (and `CB_REGISTRY_IMAGE`) are probably user-provided and
thus are favored over `CI_REGISTRY`.

[var-defaults]: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1867

## Hooks

There are two hooks, which will execute all scripts from a certain directory
from the source tree:

- The *pre-build hook* executes scripts from the directory specified by the
  variable `CB_HOOK_PRE_BUILD`. This hook runs before all other variables are
  processed and the container build is started. Therefore, this hook is suited
  to modify variables or other parts of the environment before the build.
- The *post-build hook* executes scripts from the directory specified by the
  variable `CB_HOOK_POST_BUILD`. This hook runs after the build has finished and
  before the image is tagged and possibly pushed to a registry. The build
  process automatically creates a local tag of the image in the form of
  `CB_IMAGE:CB_TAG`. This tag will be stored in the variable `CB_LOCAL_IMAGE`
  when running scripts in the *post-build hook*. If the hook is used to make
  modifications to the image, the thereby newly created image should either
  again be tagged with `CB_IMAGE:CB_TAG` or variable `CB_LOCAL_IMAGE` should be
  adjusted to container image ID or tag of the modified image. Note, that
  `CB_LOCAL_IMAGE` is only valid in the context of this hook and can not be set
  from outside.

The environment of the hooks is quite limited, since it is executed in the
container-builder container (which is based on the official *docker* image,
which is based on a minimalistic *alpine* image). Only posix
scripts (`#!/bin/sh`) are supported. However, there are no limitations for the
hook scripts to modify the running environment, e.g. install packages
via [apk][apk] and call other programms or interpreters.

[apk]: https://wiki.alpinelinux.org/wiki/Package_management

FROM docker.io/library/docker:20.10.17

RUN mkdir -p /root/.docker

RUN apk add python3

COPY template.py /
COPY build.sh /
